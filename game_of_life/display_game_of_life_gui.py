from tkinter import *
from survival import *
import time

height, width = 50,50
canvas_height, canvas_width = 750, 750

#grid_game = amorce(rd.choice(list(seeds.keys())), generate_universe((height, width)))

def draw_universe(grid_game):
    for x in range(width):
        for y in range(height):
            if grid_game[y][x] == 1:
                graphical_grid.create_rectangle(x * canvas_width/width, y * canvas_height/height, (x+1)*canvas_width /width, (y+1)*canvas_height/ height, fill = 'black')

def display_and_update_graphical_gameoflife(grid_game, nombre_etapes, pause):
    gameoflife = Tk()
    gameoflife_toplevel = Toplevel(gameoflife)
    gameoflife_toplevel.title("gameoflife")
    graphical_grid = Canvas(gameoflife_toplevel, height = canvas_height, width = canvas_height)
    graphical_grid.pack()
    for k in range(nombre_etapes):
        for x in range(width):
            for y in range(height):
                if grid_game[y][x] == 1:
                    graphical_grid.create_rectangle(x*canvas_width/width, y*canvas_height/height, (x+1)*canvas_width /width, (y+1)*canvas_height/ height, fill = 'red')
                else:
                    graphical_grid.create_rectangle(x*canvas_width/width, y*canvas_height/height, (x+1)*canvas_width /width, (y+1)*canvas_height/ height, fill = 'white')
        gameoflife_toplevel.update()
        time.sleep(pause)
        grid_game = generation(grid_game)
    gameoflife_toplevel.mainloop()

