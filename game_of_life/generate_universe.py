import numpy as np
import random as rd
import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt
import matplotlib.colors as cls

colors = ['white', 'black']
colormap = cls.ListedColormap(colors)


def generate_universe(size):
    return np.zeros(size, int)  # univers vierge

def create_seed(type_seed = "r_pentomino"):
    return [[0, 1, 1], [1, 1, 0], [0, 1, 0]]

def add_seed_to_universe_random(seed, universe, x_start = 1, y_start = 1):
    (n, m) = universe.shape
    (i, j) = (len(seed), len(seed[0]))
    colonne = rd.randint(0, m-j)  # on choisit aléatoirement la colonne dans l'univers de la colonne de gauche de la seed
    ligne = rd.randint(0, n-i)  # on choisit alétoirement la ligne dans l'univers de la ligne du haut de la seed
    universe[ligne:ligne+i, colonne:colonne+j]=seed  # on place seed dans l'univers connaissant la position aléatoire du point en haut à gauche
    return universe

def add_seed_to_universe(seed, universe, init_position_coin_gauche, x_start = 1, y_start = 1):
    (n, m) = universe.shape
    (i, j) = (len(seed), len(seed[0]))
    (x, y) = init_position_coin_gauche
    universe[y:y+i, x:x+j]=seed  # l'abscisse correspond à la colonne et l'ordonnée à la ligne
    return universe


def draw_universe(universe):
    plt.imshow(universe, cmap = cmap)
    plt.show()

def draw_universe_with_seed_random(seed, universe):
    draw_universe(add_seed_to_universe_random(seed, universe))










