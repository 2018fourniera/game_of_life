from generate_universe import *
from catalogue import *

def amorce(seed, universe):
    s = seeds[seed]
    (i, j) = (len(s), len(s[0]))
    (n, m) = universe.shape
    assert n >= i and m >= j  # on verifie que seed rentre dans universe
    return add_seed_to_universe_random(s, universe)
