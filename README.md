##Game of life 

Objectif : Programmer le MVP(Minimum Viable Product) du jeu de la vie de Conway

Liste des fichiers Python et des fonctions qu'ils contiennent :
    - catalogue.py : Contient un dictionnaire 'seeds' dans lequel sont stockés des
    motifs pour amorcer une partie de jeu de la vie.
    
    - generate_universe.py: création et affichage d'univers, contient :
        generate_universe qui renvoie un univers de taille donnéee en paramètre
        
        create_seed qui renvoie une amorce de type "r_pentomino"
        
        add_seed_to_universe_random qui ajoute aléatoirement un motif donné en 
        paramètres dans un univers donné
        
        add_seed_to_universe qui effectue la même chose mais en ajoutant le motif
        à une position donnée
        
        draw_universe qui affiche un univers donné
        
        draw_universe_with_seed qui fait la même chose en ajoutant à une position
        aléatoire un motif à l'univers
        
    - amorce.py : contient:
        amorce qui renvoie un univers en ajoutant un motif du catalogue à une
        position aléatoire
        
    - survival.py : contient :
        survival qui détermine si une cellule donnée sera vivante (renvoie 1) ou
        morte (renvoie 0) à l'étape suivante
        
        generation qui renvoie l'étape suivante d'un univers donné
        
    - simulation.py : contient :
        game_life_simulation qui renvoie l'état d'un univers après un nombre donné 
        d'itérations
        
        simulation_anime qui affiche l'évolution de l'univers avec une configuration
        initiale donnée et pendant un nombre donné d'étapes
        
        
        

